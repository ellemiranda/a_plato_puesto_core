# A PLATO PUESTO CORE

a [Sails v1](https://sailsjs.com) application

Review .env.example file

## INSTALL DOCKER AND DOCKER COMPOSE

See: https://docs.docker.com/compose/install/

## DOCKER CONTAINERS

Avalible ENVs: ``test`` 

  docker-compose -f docker-compose.yml  -f docker-compose.[ENV].yml up -d 


### ACCESS TO CONTAINER

  docker exec -it a_plato_puesto_core_[ENV] /bin/bash 


### SHOW RUNNING CONTAINERS

  docker ps

You can access to the application by the IP and port showed in the column PORTS.

### STOP ALL CONTAINERS

  docker-compose down

### STOP ONE CONTAINER 

  docker container stop [NAME|CONTAINER_ID]